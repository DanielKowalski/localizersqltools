﻿using System;
using System.Data.SqlClient;
using DBManagerLibrary.Exceptions;
using DBManagerLibrary.StandardModel;

namespace DBManagerLibrary
{
    /// <summary>
    /// Database manager.
    /// Contains methods to manage database with translations.
    /// </summary>
    public class DatabaseManager
    {
        private readonly string _connectionString;
        private readonly string _tableName;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:DBManager.DatabaseManager"/> class.
        /// </summary>
        /// <param name="connectionString">Connection string to your database.</param>
        public DatabaseManager(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new NullReferenceException("Connection string cannot be null");
            }
            _connectionString = connectionString;
            _tableName = "LocalizationRecords";
        }

       /// <summary>
       /// Insert new translation into database.
       /// </summary>
       /// <returns><c>true</c>, if translation was added, <c>false</c> othrwise.</returns>
        /// <param name="translation">Translation's model.</param>
        public bool AddTranslation(Translation translation)
        {
            if (translation == null)
            {
                throw new NullReferenceException("Translation cannot be null");
            }
            CreateTable();
            using (SqlConnection connection =
                   new SqlConnection(_connectionString))
            {
                string query = $@"INSERT INTO {_tableName}([Key],
                    [ResourceKey], [Text], [LocalizationCulture],
                    [UpdatedTimestamp]) VALUES (@key, @resourceKey, @textPL, 
                    @pl, CURRENT_TIMESTAMP), (@key, @resourceKey, @textEN, @en, 
                    CURRENT_TIMESTAMP)";
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    string tempResourceKey = translation.ResourceKey;
                    string resourceKey = translation.IsInView ?
                                                    "DotnetLocalizerSQL.Views."
                                                    + $"{tempResourceKey}"
                                                    + "DotnetLocalizerSQL"
                                                    : tempResourceKey;
                    command.Parameters.AddWithValue("@key", translation.Key);
                    command.Parameters.AddWithValue("@resourceKey",
                                                    resourceKey);
                    command.Parameters.AddWithValue("@textPL",
                                                    translation.TextInPolish);
                    command.Parameters.AddWithValue("@textEn",
                                                    translation.TextInEnglish);
                    command.Parameters.AddWithValue("@pl", "pl-PL");
                    command.Parameters.AddWithValue("@en", "en-US");

                    try
                    {
                        connection.Open();
                    }
                    catch (SqlException ex)
                    {
                        throw new DBErrorException($@"Error with connection to 
                        databse: {ex.Message}");
                    }
                    int result = -1;
                    try
                    {
                        result = command.ExecuteNonQuery();
                    }
                    catch (SqlException ex)
                    {
                        throw new DBErrorException($@"Error with sql command:
                        {ex.Message}");
                    }

                    if (result < 0)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Creates the table with columns suitable to LocalizerSQL if it doesn't exist.
        /// </summary>
        public void CreateTable()
        {
            using (SqlConnection connection = 
                   new SqlConnection(_connectionString))
            {
                string query = $@"IF NOT EXISTS (SELECT * FROM 
                                INFORMATION_SCHEMA.TABLES WHERE 
                                TABLE_NAME = '{_tableName}')
                                 BEGIN
                                    CREATE TABLE {_tableName}(
                                        ""Id"" bigint CONSTRAINT 
                                        ""PK_DataEventRecord"" NOT NULL 
                                        PRIMARY KEY IDENTITY(1,1),
                                        ""Key"" NVARCHAR(max),
                                        ""ResourceKey"" NVARCHAR(max),
                                        ""Text"" NVARCHAR(max),
                                        ""LocalizationCulture"" NVARCHAR(max),
                                        ""UpdatedTimestamp"" DATE NOT NULL
                                    );          
                                 END";
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();
                    }
                    catch (SqlException ex)
                    {
                        throw new DBErrorException($@"Error with connection to 
                        databse: {ex.Message}");
                    }
                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException ex)
                    {
                        throw new DBErrorException($@"Error with sql command: 
                        {ex.Message}");
                    }
                }
            }
        }

        //TODO Add remove and update methods
    }
}
