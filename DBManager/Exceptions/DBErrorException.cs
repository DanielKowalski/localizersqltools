﻿using System;
using System.Runtime.Serialization;

namespace DBManagerLibrary.Exceptions
{
    public class DBErrorException : System.Exception
    {
        public DBErrorException() : base() {}
        public DBErrorException(string message) : base(message) {}
        public DBErrorException(string message, Exception inner) 
            : base(message, inner) {}

        protected DBErrorException(SerializationInfo info, 
                                   StreamingContext context) : base(info, 
                                                                    context) {}
    }
}
