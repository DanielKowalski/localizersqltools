﻿using System.ComponentModel.DataAnnotations;

namespace DBManagerLibrary.StandardModel
{
    /// <summary>
    /// Translation. 
    /// This is how model for yout translation should look.
    /// </summary>
    public class Translation
    {
        [Required(ErrorMessage = "Required")]
        public string Key { get; set; }

        [Required(ErrorMessage = "Required")]
        public string ResourceKey { get; set; }

        [Required(ErrorMessage = "Required")]
        public string TextInPolish { get; set; }

        [Required(ErrorMessage = "Required")]
        public string TextInEnglish { get; set; }

        [Required(ErrorMessage = "Required")]
        public bool IsInView { get; set; }
    }
}
