﻿using System.Net.Mail;

namespace MailSenderLibrary
{
    /// <summary>
    /// Message builder.
    /// </summary>
    public class MessageBuilder
    {
        private readonly MailMessage _message;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:MailSender.MessageBuilder"/> class.
        /// </summary>
        public MessageBuilder()
        {
            _message = new MailMessage
            {
                IsBodyHtml = true
            };
        }

        /// <summary>
        /// Builds the subject.
        /// </summary>
        /// <param name="subject">Subject.</param>
        public void BuildSubject(string subject)
        {
            if (string.IsNullOrWhiteSpace(subject))
            {
                throw new System.ArgumentException("Subject cannot be null");
            }
            _message.Subject = subject;
        }

        /// <summary>
        /// Builds from address.
        /// </summary>
        /// <param name="fromAddress">From address.</param>
        public void BuildFromAddress(string fromAddress)
        {

            if (string.IsNullOrWhiteSpace(fromAddress))
            {
                throw new System.ArgumentException("From user's address cannot"
                                                   + " be null");
            }
            _message.From = new MailAddress(fromAddress);
        }

        /// <summary>
        /// Builds addresseses' addresses from array of strings.
        /// </summary>
        /// <param name="addressesesAddresses">Addresseses' addresses.</param>
        public void BuildToAddresses(string[] addressesesAddresses)
        {
            for (int i = 0; i < addressesesAddresses.Length; i++)
            {
                string toAddress = addressesesAddresses[i];
                if (string.IsNullOrWhiteSpace(toAddress))
                {
                    throw new System.ArgumentException("To user's address "
                                                       + "cannot be null");
                }
                _message.To.Add(new MailAddress(toAddress));
            }
        }

        /// <summary>
        /// Builds message's body from array of strings.
        /// Each string in array is one line in body.
        /// You can use html tags in body.
        /// </summary>
        /// <param name="bodyLines">Body lines.</param>
        public void BuildBody(string[] bodyLines)
        {
            string body = "";
            for (int i = 0; i < bodyLines.Length; i++)
            {
                body += $"{bodyLines[i]}<br />";
            }
            _message.Body = body;
        }

        /// <summary>
        /// Build instance of mail's message.
        /// </summary>
        /// <returns>The build.</returns>
        public MailMessage Build()
        {
            return _message;
        }
    }
}
