﻿using System.Net;
using System.Net.Mail;

namespace MailSenderLibrary
{
    /// <summary>
    /// Mail sender.
    /// </summary>
    public class MailSender
    {
        private readonly int _port;
        private readonly string _host;
        private readonly MailMessage _message;
        private readonly NetworkCredential _credentials;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:MailSender.MailSender"/> class.
        /// </summary>
        /// <param name="port">SMTP port.</param>
        /// <param name="host">SMTP server .</param>
        /// <param name="credentials">Mail's credential.</param>
        /// <param name="message">Mail's Message.</param>
        public MailSender(int port, string host, NetworkCredential credentials,
                          MailMessage message)
        {
            _port = port;
            _host = host;
            _credentials = credentials;
            _message = message;
        }

        /// <summary>
        /// Sends the mail.
        /// </summary>
        public void SendMail()
        {
            using (SmtpClient client = new SmtpClient(_host))
            {
                client.Port = _port;
                client.UseDefaultCredentials = false;
                client.Credentials = _credentials;
                client.EnableSsl = true;
                client.Send(_message);
            }
        }
    }
}
