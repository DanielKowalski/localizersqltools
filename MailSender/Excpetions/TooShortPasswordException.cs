﻿namespace MailSenderLibrary.Excpetions
{
    /// <summary>
    /// Too short password exception.
    /// </summary>
    public class TooShortPasswordException : System.Exception
    {
        public TooShortPasswordException() : base() {}
        public TooShortPasswordException(string message) : base(message) {}
        public TooShortPasswordException(string message, System.Exception inner)
            : base(message, inner) {}

        protected TooShortPasswordException(System.Runtime.Serialization.
                                            SerializationInfo info,
                                            System.Runtime.Serialization.
                                            StreamingContext context) {}
    }
}
