﻿using System;
using System.Net;
using MailSenderLibrary.Excpetions;

namespace MailSenderLibrary
{
    /// <summary>
    /// Credentials builder.
    /// </summary>
    public class CredentialsBuilder
    {
        private readonly NetworkCredential _credentials;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:MailSender.CredentialsBuilder"/> class.
        /// </summary>
        public CredentialsBuilder()
        {
            _credentials = new NetworkCredential();
        }

        /// <summary>
        /// Builds the name of the user.
        /// </summary>
        /// <param name="userName">Username.</param>
        public void BuildUserName(string userName)
        {
            if (string.IsNullOrWhiteSpace(userName))
            {
                throw new NullReferenceException("Username cannot be null!");
            }
            _credentials.UserName = userName;
        }

        /// <summary>
        /// Builds the password.
        /// </summary>
        /// <param name="password">Password.</param>
        public void BuildPassword(string password) 
        {
            if (string.IsNullOrWhiteSpace(password))
            {
                throw new NullReferenceException("Password cannot be null!");
            }
            if (password.Length < 8) 
            {
                throw new TooShortPasswordException("Password must have at "
                                                    + "least 8 characters!");
            }
            _credentials.Password = password;
        }

        /// <summary>
        /// Build instance of <see cref="NetworkCredential"/>.
        /// </summary>
        /// <returns>The build.</returns>
        public NetworkCredential Build()
        {
            return _credentials;
        }
    }
}
